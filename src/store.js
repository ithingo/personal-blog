import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    posts: [
      {
        id: 5436,
        title: "first post",
        body: "first ppost body, blah-blah-blah"
      },
      {
        id: 453453453,
        title: "second post",
        body: "second ppost body, blah-blah-blah"
      },
      {
        id: 23421,
        title: "third post",
        body: "third ppost body, blah-blah-blah"
      }
    ],
    postToJump: {}
  },
  getters: {
    getPosts: state => state.post,
    getPostById: state => id => state.posts.find(post => post.id === id),
    getPostToJump: state => state.postToJump
  },
  mutations: {
    ADD_POST: (state, post) => state.posts.push(post),
    DELETE_POST: (state, post) => {
      state.posts.forEach((item, index, arr) => {
        if (item.id === post.id) arr.splice(index, 1)
      })
    },
    CHANGE_POST: (state, post) => {
      state.posts.forEach((item, index, arr) => {
        if (item.id === post.id) arr.splice(index, 1)
      })
    },
    SAVE_POST: (state, post) => state.posts.push(post),
    SET_POST_TO_JUMP: (state, post) => (state.postToJump = post),
    REMOVE_POST_TO_JUMP: state => (state.postToJump = {})
  },
  actions: {
    addPost: (state, post) => state.commit("ADD_POST", post),
    deletePost: (state, post) => state.commit("DELETE_POST", post),
    changePost: (state, post) => state.commit("CHANGE_POST", post),
    savePost: (state, post) => state.commit("SAVE_POST", post),
    setPostToJump: (state, post) => state.commit("SET_POST_TO_JUMP", post),
    removePostToJump: (state, post) => state.commit("REMOVE_POST_TO_JUMP", post)
  }
});
