import Vue from "vue";

import Vuelidate from 'vuelidate';
import './plugins/vuetify'
import '@babel/polyfill'

import App from "./App.vue";
import router from "./router";
import store from "./store";

Vue.config.productionTip = false;

// for form validation
Vue.use(Vuelidate);

// config with mock-api url (test response)
Vue.config.formApiUrl = process.env.FORM_API_URL;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
