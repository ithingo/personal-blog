'use strict'

module.exports = {
  NODE_ENV: '"production"',
  FORM_API_URL: '"https://www.mocky.io/v2/5adb5a8c2900002b003e3df1"', // Success
  //FORM_API_URL: '"https://www.mocky.io/v2/5ade0bf2300000272b4b29b9"', // Failure
}

/** Success example
 * {
  "success": true,
  "errors": []
}
 */

/** Failure example
 * {
  "success": false,
  "errors": [
    {
      "field": "firstName",
      "message": null
    },
    {
      "field": "lastName",
      "message": null
    },
    {
      "field": "email",
      "message": "Email is not valid email address."
    },
    {
      "field": "additionalInfo",
      "message": "Max. 1000 characters."
    }
  ]
}
 */