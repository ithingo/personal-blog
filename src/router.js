import Vue from "vue";
import Router from "vue-router";

import HomePage from "./views/HomePage";

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "/",
      name: "home",
      component: HomePage
    },
    {
      path: "/blog",
      name: "blog",
      // route level code-splitting
      // this generates a separate chunk (posts.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () =>
        import(/* webpackChunkName: "posts" */ "./views/BlogPage.vue")
    },
    {
      path: "/posts/:id",
      name: "posts",
      component: () =>
        import(/* webpackChunkName: "posts/:id" */ "./views/PostPage.vue")
    }
  ]
});
